const apiUrl = 'http://localhost:5001/api/';
//const apiUrl = 'https://digitalsignatureapi.royalfinances.ca/api/'; // Royal
//const apiUrl = 'https://digitalsignatureapi.creditbook.ca/api/'; // Royal DEMO
//const apiUrl = 'https://digitalsignatureapi.soniccash500.com/api/'; // Sonic
//const apiUrl = 'https://digitalsignatureapi.pretc.ca/api/'; // PretC
//const apiUrl = 'https://digitalsignatureapi.alphaloans.ca/api/'; // Alpha
//const apiUrl = 'https://digitalsignatureapi.expresscreditplus.com/api/'; // Express Credit
//const apiUrl = 'https://digitalsignatureapi.solutions500.com/api/'; // Solutions500
//const apiUrl = 'http://digitalsignatureapi.pretcash2000.com/api/'; // Pret Cash 2000
//const apiUrl = 'https://digitalsignatureapi.24hrcash.ca/api/'; // 24hr Cash
//const apiUrl = 'https://digitalsignatureapi.financedirect.ca/api/'; // Finance Directe

/*
const requestHeadersContent = () => ({
    "Content-Type": "application/json",
});
*/

export const getContract = async (guid) => {
    const requestUrl = new URL(`contract/${guid}`, apiUrl);
    const response = await fetch(requestUrl);
    return await response.json();
}

export const signContract = async (guid, clientIp, sigBase64, browser) => {
    //const requestUrl = new URL(`contract/${guid}?dateOfBirth=${dateOfBirth}`, apiUrl);
    const requestUrl = new URL(`contract/${guid}/${clientIp}`, apiUrl);
    //const requestUrl = new URL("contract", apiUrl);
    var _body = {
        sigBase64: sigBase64,
        browser: browser
    };

    try {
        const response = await fetch(requestUrl, {
            method: 'PUT',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(_body)
        });
        return response.json();
    } catch (err) {
        return { errorCode: err };
    }
}