import React from 'react';
import { CheckCircleOutline } from '@material-ui/icons';

const SuccessPage = () => {
    return <div style={{height: "100vh", display: "flex", justifyContent: "center", alignItems: "center"}}>
        <span>Contract signed!</span><CheckCircleOutline size={100}/>
    </div>
}

export default SuccessPage;