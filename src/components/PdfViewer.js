import React, { useState, useEffect } from 'react';
import GoogleDocsViewer from 'react-google-docs-viewer';
//import { Document, Page } from 'react-pdf';
// import { Document, Page, pdfjs } from "react-pdf";
//import samplePDF from "./franseca_fortune_000016_Contrat.pdf";
// pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
// import { Document, Page } from 'react-pdf/dist/esm/entry.webpack';
// import Iframe from 'react-iframe'

const PdfViewer = (props) => {
    const path = props.path;
    //const url = "https://cors-anywhere.herokuapp.com/sloanapi.pretcash2000.com/ClientContracts/franseca_fortune_000003_Contrat.pdf";
    const url = path.replace("//", "//cors-anywhere.herokuapp.com/");
    const [numPages, setNumPages] = useState(null);
    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
    }

    return (

        <GoogleDocsViewer
            width="100%"
            fileUrl={path} {...props}
        />

        // <Iframe url={"https://docs.google.com/gview?url=" + path + "&embedded=true"}
        //     id="myId"
        //     className="responsive-iframe"
        //     display="initial"
        //     position="relative" />
    )
};

export default PdfViewer;