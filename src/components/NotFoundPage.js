import React from 'react';
import ErrorIcon from '@material-ui/icons/Error';

const NotFoundPage = () => {
    return <div style={{ height: "100vh", display: "flex", justifyContent: "center", alignItems: "center" }}>
        <span>This is not a valid contract URL</span><ErrorIcon size={100} />
    </div>
}

export default NotFoundPage;